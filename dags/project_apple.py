import datetime;
from datetime import timedelta
import requests
import json
import airflow
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from jsontomongo import JsonToMongoOperator

startUrl = "https://financialmodelingprep.com/api/v3"
apiKey = "?apikey=96cfba5b29806d50a7547c26edf8d0dc"

def getRequest(route):
    request = startUrl + route
    r = requests.get(request + apiKey)
    return r.json()
    
def getAppleProfile():
    return getRequest("/profile/AAPL")

def getAppleRating():
    return getRequest("/rating/AAPL")


def write_company_profile_file():
    data = {
        "profile": getAppleProfile(),
        "timestamp": datetime.datetime.now().timestamp()
        }
    with open('/tmp/profileDataApple.json', 'w') as outfile:
        json.dump(data, outfile)

def write_company_rating_file():
    data = {
        "profile": getAppleRating(),
        "timestamp": datetime.datetime.now().timestamp()
        }
        
    with open('/tmp/ratingDataApple.json', 'w') as outfile:
        json.dump(data, outfile)
    
args = {
    'owner': 'Leanna',
    'start_date': airflow.utils.dates.days_ago(2),
}

dag = DAG(
  dag_id='schedule_every_minute',
  schedule_interval=timedelta(minutes = 1), #..every minute
  default_args=args,
)

tcreateProfile = PythonOperator(
    task_id='create_company_profile',
    python_callable=write_company_profile_file,
    dag=dag,
)
tcreateRating = PythonOperator(
    task_id='create_company_rating',
    python_callable=write_company_rating_file,
    dag=dag,
)

tloadProfile = JsonToMongoOperator(
    task_id='insert_company_profile',
    file_to_load='/tmp/profileDataApple.json',
    mongouser = 'root',
    mongopass = 'root',
    mongodb = 'project_apple',
    mongocollection = 'company_profile',
    mongoserver='localhost:27017/',
    dag=dag,
)

tloadRating = JsonToMongoOperator(
    task_id='insert_company_rating',
    file_to_load='/tmp/ratingDataApple.json',
    mongouser = 'root',
    mongopass = 'root',
    mongodb = 'project_apple',
    mongocollection = 'company_rating',
    mongoserver='localhost:27017/',
    dag=dag,
)

[tcreateProfile, tcreateRating] >> tloadProfile >> tloadRating