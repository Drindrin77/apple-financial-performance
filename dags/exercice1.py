"""
Let's import the libraries that we need.
timedelta from datetime
DAG from airflow.models
BashOperator from airflow.operators.bash_operator
"""
from datetime import timedelta

import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator


"""
Create a dictionary called args that states the:
id
owner
start_date
schedule_interval to daily
"""

args = {
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(2),
    'schedule_interval': 'daily'
}


"""
Instantiate a DAG class with the following arguments:
id
default_args
schedule_interval
"""

dag = DAG(
    'exercice1',
    default_args=args,
    description='DAG exercice 1',
    schedule_interval=timedelta(days=1),
)


"""
Create 4 BashOperator instance that will echo {{task_instance_key_str}}

"""

t1 = BashOperator(
    task_id='print_task_instance_key1',
    bash_command='echo {{task_instance_key_str}}',
    dag=dag,
)

t2 = BashOperator(
    task_id='print_task_instance_key2',
    bash_command='echo {{task_instance_key_str}}',
    dag=dag,
)

t3 = BashOperator(
    task_id='print_task_instance_key3',
    bash_command='echo {{task_instance_key_str}}',
    dag=dag,
)

t4 = BashOperator(
    task_id='print_task_instance_key4',
    bash_command='echo {{task_instance_key_str}}',
    dag=dag,
)

"""
Finally create a final BashOperator instance that will simply echo CIAO :)
"""


tfinal = BashOperator(
    task_id='final_bashOperator1',
    bash_command='echo "CIAO :)"',
    dag=dag,
)


[t1, t2, t3, t4] >> tfinal